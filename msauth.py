import requests
import msal
from msal_extensions import *
import sys
import configparser
import pandas as pd
from collections import namedtuple
import jwt
import json
import datetime
import re
import numpy as np

this = sys.modules[__name__]
config = configparser.ConfigParser()
config.read('config.cfg')
this.settings = config['azure']


# mine
authority = f'https://login.microsoftonline.com/{this.settings["tenantId"]}'
scope = ['https://graph.microsoft.com/.default']


class GraphAPI:
    """
    A simple way to interact with the MSAL authentication system using new
    and cached credentials (so we don't have to go through the authorization
    flow every time
    """

    def __init__(self, username):
        self.username = username
        self.token = self.get_access_token(username)
        self.tokenExp = self.msal_jwt_expiry(self.token)
        self.uri = 'https://graph.microsoft.com/v1.0'
        self.headers = {'Authorization': f'Bearer {self.token}',
                        'Content-Type': 'application/json'}

    def msal_persistence(self, location, fallback_to_plaintext=False):
        """Build a suitable persistence instance based your current OS"""
        if sys.platform.startswith('win'):
            return FilePersistenceWithDataProtection(location)
        if sys.platform.startswith('darwin'):
            return KeychainPersistence(location, "my_service_name", "my_account_name")
        return FilePersistence(location)

    def msal_cache_accounts(self, client_id, authority):
        # Accounts
        persistence = self.msal_persistence("token_cache.bin")
        cache = PersistedTokenCache(persistence)
        app = msal.PublicClientApplication(
            client_id=client_id, authority=authority, token_cache=cache)
        accounts = app.get_accounts()
        return accounts

    def msal_delegated_refresh(self, client_id, scope, authority, account):
        persistence = self.msal_persistence("token_cache.bin")
        cache = PersistedTokenCache(persistence)
        app = msal.PublicClientApplication(
            client_id=client_id, authority=authority, token_cache=cache)
        result = app.acquire_token_silent_with_error(
            scopes=scope, account=account)
        return result

    def msal_delegated_refresh_force(self, client_id, scope, authority, account):
        persistence = self.msal_persistence("token_cache.bin")
        cache = PersistedTokenCache(persistence)
        app = msal.PublicClientApplication(
            client_id=client_id, authority=authority, token_cache=cache)
        result = app.acquire_token_silent_with_error(
            scopes=scope, account=account, force_refresh=True)
        return result

    def msal_delegated_device_flow(self, client_id, scope, authority):
        print("Initiate Device Code Flow to get an AAD Access Token.")
        print("Open a browser window and paste in the URL below and then enter the Code. CTRL+C to cancel.")
        persistence = self.msal_persistence("token_cache.bin")
        cache = PersistedTokenCache(persistence)
        app = msal.PublicClientApplication(client_id=client_id, authority=authority, token_cache=cache)
        flow = app.initiate_device_flow(scopes=scope)
        if "user_code" not in flow:
            raise ValueError("Fail to create device flow. Err: %s" % json.dumps(flow, indent=4))
        sys.stdout.flush()
        result = app.acquire_token_by_device_flow(flow)
        return result

    def msal_jwt_expiry(self, access_token):
        alg = jwt.get_unverified_header(access_token)['alg']
        decoded_access_token = jwt.decode(access_token, algorithms=[alg], options={"verify_signature": False})
        # token expiry
        token_expiry = datetime.datetime.utcfromtimestamp(decoded_access_token["exp"])
        return token_expiry

    def get_access_token(self, username):
        accounts = self.msal_cache_accounts(client_id=this.settings['clientId'],
                                            authority=authority)
        if accounts:
            for account in accounts:
                if account['username'].lower() == username.lower():
                    my_account = account
                    result = self.msal_delegated_refresh(this.settings['clientId'], scope, authority, my_account)
                    if result is None:
                        # Get a new Access Token using the Device Code Flow
                        result = self.msal_delegated_device_flow(this.settings['clientId'], scope, authority)
                    else:
                        if result["access_token"]:
                            self.msal_jwt_expiry(result["access_token"])
        else:
            print('there are no accounts, using msal_delegated_device_flow')
            # get new access token using the device flow code
            result = self.msal_delegated_device_flow(this.settings['clientId'], scope, authority)
            if result["access_token"]:
                # token expiry
                self.msal_jwt_expiry(result["access_token"])
        return result['access_token']

    def get(self, endpoint, headers=None):
        results = requests.get(url=f'{self.uri}{endpoint}',
                               headers=self.headers).json()
        return results

    def post(self, endpoint, data, headers=None):
        results = requests.post(url=f'{self.uri}{endpoint}',
                                data=json.dumps(data),
                                headers=self.headers)
        return results

    def patch(self, endpoint, data, headers=None):
        results = requests.patch(url=f'{self.uri}{endpoint}',
                                 data=data,
                                 headers=self.headers).json()
        return results


class Graph(GraphAPI):
    def send_email(self,
                   recipients: list[str],
                   subject: str,
                   content: str,
                   is_html: bool,
                   cc_recipients: list[str] = ['alex'],
                   bcc_recipients: list[str] = ['salesforce']):
        data = {'message': {'subject': subject,
                            'body': {'contentType': 'HTML' if is_html else 'text',
                                     'content': content},
                            'toRecipients': [],
                            "ccRecipients": [],
                            "bccRecipients": []
                            },
                "saveToSentItems": "true"
                }
        sf_email = 'emailtosalesforce@2brrs8u7izixam1izyz0sgtswiz9zkl7l072c2kyh7sicoxe50.u-lwrrmao.na226.le.salesforce.com'
        for r in recipients:
            data['message']['toRecipients'].append({'emailAddress': {'address': r}})
        for r in bcc_recipients:
            if r == 'salesforce':
                data['message']['bccRecipients'].append({'emailAddress': {'address': sf_email}})
            elif r == 'alex':
                data['message']['bccRecipients'].append({'emailAddress': {'address': 'alexdeleon@adt.com'}})
            else:
                data['message']['bccRecipients'].append({'emailAddress': {'address': r}})
        for r in cc_recipients:
            if r == 'alex':
                data['message']['ccRecipients'].append({'emailAddress': {'address': 'alexdeleon@adt.com'}})
            else:
                data['message']['ccRecipients'].append({'emailAddress': {'address': r}})
        return self.post(endpoint='/me/sendmail', data=data)

    def get_shared_file(self, fileName):
        File = namedtuple('File', ['name', 'driveId', 'id'])
        # endpoint = f'/me/drive/sharedWithMe?$search={fileName}&$select=name,id,remoteItem'
        endpoint = f'/me/drive/sharedWithMe'
        shared_files = self.get(endpoint).get('value', [])
        print(shared_files)
        print(f'searching for {fileName} in shared_files')
        for f in shared_files:
            # if can find desired name in file name
            if re.search(fr'(?ims){fileName}', f['name']):
                return File(name=f['name'],
                            driveId=f['remoteItem']['parentReference']['driveId'],
                            id=f['id'])
        # return None if no matching file is found
        return None

    def create_worksheet_df(self, values: list[str]) -> pd.DataFrame:
        cols = [c.lower() for c in values[0]]
        print(cols)
        rows = values[1:] or ['' for c in cols]
        df = pd.DataFrame(columns=cols, data=rows)
        # remove all rows from the end df where all values in row are empty strings and
        # now rows afterwards have any non-empty values
        df = df.loc[~(df == '').all(axis=1)]
        non_empty_indices = ~df.astype(str).eq('').all(axis=1).cumprod()
        df = df.loc[:non_empty_indices.idxmax()]
        # df = df.loc[~(df.astype(str) == '').all(axis=1)]
        # reindex to match row number
        if type(cols[0] == str):
            row_offset = 1
            df.index = np.arange(row_offset, len(df) + row_offset)
        return df

    def get_worksheet(self, workbook_name, worksheet_name):
        Worksheet = namedtuple('Worksheet', ['driveId',
                                             'workbook',
                                             'workbookId',
                                             'worksheet',
                                             'endpoint',
                                             'values',
                                             'df'])
        workbook = self.get_shared_file(workbook_name)
        endpoint = f'/me/drives/{workbook.driveId}/items/{workbook.id}/' \
                   f'workbook/worksheets/{worksheet_name}'
        used_range = self.get(endpoint=f'{endpoint}/usedRange/').get('text', [])
        df = self.create_worksheet_df(used_range)
        return Worksheet(driveId=workbook.driveId,
                         workbook=workbook.name,
                         workbookId=workbook.id,
                         worksheet=worksheet_name,
                         endpoint=endpoint,
                         values=used_range,
                         df=df)

    def update_cell(self, worksheet, row, col, value, value_type='String'):
        self.patch(endpoint=f'{worksheet.endpoint}/cell(row={row},column={col})',
                   data=json.dumps({'values': [[value]],
                                    'valueTypes': [[value_type]]}))


global graph
graph = Graph(this.settings.get('username', None))

# graphy = Graph(this.settings.get('username',None))

# expiry = graphy.tokenExp
# print(expiry)
# workbook_name = 'Jesse Ramsey Appointment tracker and call list 072623draft.xlsx'
# # worksheet_name = 'ADT SECURITY CALL LIST'
# worksheet_name = 'call email test'
# worksheet = graphy.get_worksheet(workbook_name, worksheet_name)

# def sendEmailTemplate(recipient, template, name=None, customer_id=None):
#     promo_link = f'https://adtsolar.com/adt/selfgen/30/?employee_name=Jesse%20Ramsey'
#     if customer_id:
#         promo_link += f'&adt_customer_number={customer_id}'
#     intro = f'Hi {name}!' if name else 'Hi!'
#     match template:
#         case 'promo':
#             subj = 'LIMITED OFFER - SAVE $30 every month'
#             file = './emails/promo.html'
#         case 'referral':
#             subj = 'ADT Solar Referral App: Earn per referral'
#             file = './emails/questionaire.html'
#         case 'questionaire':
#             subj = 'Quick Questionnaire: Rising Electricity Costs? Unplanned Power Outages?'
#             file = './emails/promo.html'
#         case _:
#             file = None
#     # get text from file and fill any fields
#     body = open(file).read().format(**locals())
#     send_mail(subject=subj, body=body, recipient=recipient)

