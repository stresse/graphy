# import json
# import sys
# import configparser
# import pandas as pd
# import numpy as np
# import re
# from collections import namedtuple
# import smsitai
# from msal_authentication import GraphAPI
#
#
# class Email(GraphAPI):
#     def send_email(self,
#                    recipients: list[str],
#                    subject: str,
#                    content: str,
#                    is_html: bool,
#                    cc_recipients: list[str] = ['alex'],
#                    bcc_recipients: list[str] = ['salesforce'],
#                    save_to_sent: bool = True):
#         data = {'message': {'subject': subject,
#                             'body': {'contentType': 'HTML' if is_html else 'text',
#                                      'content': content},
#                             'toRecipients': [],
#                             "ccRecipients": [],
#                             "bccRecipients": []
#                             },
#                 "saveToSentItems": str(save_to_sent).lower()
#                 }
#         sf_email = 'emailtosalesforce@2brrs8u7izixam1izyz0sgtswiz9zkl7l072c2kyh7sicoxe50.u-lwrrmao.na226.le.salesforce.com'
#         for r in recipients:
#             data['message']['toRecipients'].append({'emailAddress': {'address': r}})
#         for r in bcc_recipients:
#             if r == 'salesforce':
#                 data['message']['bccRecipients'].append({'emailAddress': {'address': sf_email}})
#             elif r == 'alex':
#                 data['message']['bccRecipients'].append({'emailAddress': {'address': 'alexdeleon@adt.com'}})
#             else:
#                 data['message']['bccRecipients'].append({'emailAddress': {'address': r}})
#         for r in cc_recipients:
#             if r == 'alex':
#                 data['message']['ccRecipients'].append({'emailAddress': {'address': 'alexdeleon@adt.com'}})
#             else:
#                 data['message']['ccRecipients'].append({'emailAddress': {'address': r}})
#         return self.post(endpoint='/me/sendmail', data=data)
#
#     def get_shared_file(self, fileName):
#         File = namedtuple('File', ['name', 'driveId', 'id'])
#         # endpoint = f'/me/drive/sharedWithMe?$search={fileName}&$select=name,id,remoteItem'
#         endpoint = f'/me/drive/sharedWithMe'
#         shared_files = self.get(endpoint).get('value', [])
#         print(shared_files)
#         print(f'searching for {fileName} in shared_files')
#         for f in shared_files:
#             # if can find desired name in file name
#             if re.search(fr'(?ims){fileName}', f['name']):
#                 return File(name=f['name'],
#                             driveId=f['remoteItem']['parentReference']['driveId'],
#                             id=f['id'])
#         # return None if no matching file is found
#         return None
#
#     def create_worksheet_df(self, values: list[str]) -> pd.DataFrame:
#         cols = [c.lower() for c in values[0]]
#         print(cols)
#         rows = values[1:] or ['' for c in cols]
#         df = pd.DataFrame(columns=cols, data=rows)
#         # remove all rows from the end df where all values in row are empty strings
#         df = df.loc[~(df.astype(str) == '').all(axis=1)]
#         # reindex to match row number
#         if type(cols[0] == str):
#             row_offset = 1
#             df.index = np.arange(row_offset, len(df) + row_offset)
#         return df
#
#     def get_worksheet(self, workbook_name, worksheet_name):
#         Worksheet = namedtuple('Worksheet', ['driveId',
#                                              'workbook',
#                                              'workbookId',
#                                              'worksheet',
#                                              'endpoint',
#                                              'values',
#                                              'df'])
#         workbook = self.get_shared_file(workbook_name)
#         endpoint = f'/me/drives/{workbook.driveId}/items/{workbook.id}/' \
#                    f'workbook/worksheets/{worksheet_name}'
#         used_range = self.get(endpoint=f'{endpoint}/usedRange/').get('text', [])
#         df = self.create_worksheet_df(used_range)
#         return Worksheet(driveId=workbook.driveId,
#                          workbook=workbook.name,
#                          workbookId=workbook.id,
#                          worksheet=worksheet_name,
#                          endpoint=endpoint,
#                          values=used_range,
#                          df=df)
#
#     def update_cell(self, worksheet, row, col, value, value_type='String'):
#         self.patch(endpoint=f'{worksheet.endpoint}/cell(row={row},column={col})',
#                    data=json.dumps({'values': [[value]],
#                                     'valueTypes': [[value_type]]}))
#
#
