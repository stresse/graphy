import json
import sys
import configparser
import pandas as pd
import numpy as np
import re
from collections import namedtuple
import smsitai

import msauth
from msauth import GraphAPI


class File:
    def __init__(self, data):
        self.data = data

    @property
    def name(self):
        return self.data.get('name', None)

    @property
    def id(self):
        return self.data.get('id', None)

    @property
    def remote_item(self):
        return self.data.get('remoteItem', {})

    @property
    def parent_reference(self):
        if self.remote_item:
            return self.remote_item.get('parentReference', {})
        else:
            return self.data.get('parentReference', {})

    @property
    def type(self):
        excel_mimes = [
            'application/vnd.ms-excel',
            'application/msexcel',
            'application/x-msexcel',
            'application/x-ms-excel',
            'application/x-excel',
            'application/x-dos_ms_excel',
            'application/xls',
            'application/x-xls',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ]
        full_type = self.data.get('file', {}).get('mimeType', '')
        # if full_type is in excel_types return 'workbook'
        if full_type in excel_mimes:
            return 'workbook'
        else:
            return 'unknown'

    @property
    def drive_id(self):
        return self.parent_reference.get('driveId', None)

    @property
    def workbook(self):
        if self.type == 'workbook':
            endpoint = f'/me/drives/{self.drive_id}/items/{self.id}/workbook'
            data = msauth.graph.get(endpoint=endpoint)
            return Workbook(data)
            # return Workbook(self.id, self.drive_id)


class Worksheet:
    def __init__(self, data):
        self.data = data
        self.endpoint = data.get('@odata.id')

    @property
    def cell_count(self):
        endpoint = f'{self.endpoint}/usedRange?$select=cellCount'
        return msauth.graph.get(endpoint).get('cellCount', None)

    @property
    def column_count(self):
        endpoint = f'{self.endpoint}/usedRange?$select=columnCount'
        return msauth.graph.get(endpoint).get('columnCount', None)

    @property
    def id(self):
        return self.data.get('id','')

    @property
    def name(self):
        return self.data.get('name','')

    @property
    def used_range(self):
        endpoint = f'{self.endpoint}/usedRange'
        print(endpoint)
        response = msauth.graph.get(endpoint)
        # print(response)
        return response

    @property
    def values(self):
        endpoint = f'{self.endpoint}/usedRange?$select=text'
        response = msauth.graph.get(endpoint)
        return response.get('text')

    def df(self, column_count=1):
        all_values = self.values
        print(f'there are {len(all_values)} values')
        columns = self.values[:column_count]
        print(columns)
        columns = [list(map(str.lower, col)) for col in self.values[:column_count]]
        # get all values in all_values after the index value of column_count
        values = self.values[column_count:]
        print(values[-1])
        print(f'there are {len(values)} values')
        df = pd.DataFrame(columns=columns, data=values)
        empty_rows = df.apply(lambda row: row.str.strip().eq('')).all(axis=1)
        if empty_rows.all():
            return pd.DataFrame()  # Return an empty DataFrame if all rows are empty

        # Find the index of the first row with non-empty values after the empty rows
        first_nonempty_row = empty_rows[::-1].idxmin()

        if first_nonempty_row == 0:
            return df

        # Remove the empty rows if there are no non-empty rows after them
        df_filtered = df.iloc[:first_nonempty_row + 1]

        return df_filtered



class Workbook:
    def __init__(self, data):
        self.data = data
        self.endpoint = data.get('@odata.id')
        print(self.endpoint)
        # self.id = fileId
        # self.drive_id = driveId
        # self.base_endpoint = f'/me/drives/{self.drive_id}/items/{self.id}/workbook'
        # print(f'self.base_endpoint={self.base_endpoint}')

    @property
    def worksheets(self):
        response = msauth.graph.get(endpoint=f'{self.endpoint}worksheets').get('value',{})
        return [Worksheet(d) for d in response]


class Drive(GraphAPI):
    selections = '$select=name,id,parentReference,file'

    @property
    def files(self):
        endpoint = f'/me/drive/root/children?{self.selections}'
        response = self.get(endpoint).get('value', [])
        return [File(d) for d in response]

    @property
    def shared_files(self):
        endpoint = f'/me/drive/sharedWithMe?${self.selections}'
        response = self.get(endpoint).get('value', [])
        return [File(d) for d in response]

    def find_files(self, file_name):
        endpoint = f'/me/drive/search(q=\'{file_name}\')?{self.selections}'
        response = self.get(endpoint).get('value', [])
        return [File(d) for d in response]

    def find_file(self,file_name):
        files = self.find_files(file_name)
        for f in files:
            if re.match(fr'(?ims){file_name}', f.name):
                return f

    def create_worksheet_df(self, values: list[str]) -> pd.DataFrame:
        cols = [c.lower() for c in values[0]]
        print(cols)
        rows = values[1:] or ['' for c in cols]
        df = pd.DataFrame(columns=cols, data=rows)
        # remove all rows from the end df where all values in row are empty strings
        df = df.loc[~(df.astype(str) == '').all(axis=1)]
        # reindex to match row number
        if type(cols[0] == str):
            row_offset = 1
            df.index = np.arange(row_offset, len(df) + row_offset)
        return df

    def get_worksheet(self, workbook_name, worksheet_name):
        Worksheet = namedtuple('Worksheet', ['driveId',
                                             'workbook',
                                             'workbookId',
                                             'worksheet',
                                             'endpoint',
                                             'values',
                                             'df'])
        workbook = self.get_shared_file(workbook_name)
        endpoint = f'/me/drives/{workbook.driveId}/items/{workbook.id}/' \
                   f'workbook/worksheets/{worksheet_name}'
        used_range = self.get(endpoint=f'{endpoint}/usedRange/').get('text', [])
        df = self.create_worksheet_df(used_range)
        return Worksheet(driveId=workbook.driveId,
                         workbook=workbook.name,
                         workbookId=workbook.id,
                         worksheet=worksheet_name,
                         endpoint=endpoint,
                         values=used_range,
                         df=df)

    def update_cell(self, worksheet, row, col, value, value_type='String'):
        self.patch(endpoint=f'{worksheet.endpoint}/cell(row={row},column={col})',
                   data=json.dumps({'values': [[value]],
                                    'valueTypes': [[value_type]]}))
